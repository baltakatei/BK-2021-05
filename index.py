#!/usr/bin/env python3
# Desc: Generates index for use by search.py
# Input: Text files in `-i`-specified directory, arguments specifying paths for
#   dictionary, metadata, and postings.
# Output: dictionary, metadata, and postings files.

import collections
import getopt
import codecs
import struct
import timeit
import pickle
import math
import nltk
import sys
import os
import re
import json
import time # DEBUG
import pprint # DEBUG

LIMIT = None                # (for testing) to limit the number of documents indexed
IGNORE_STOPWORDS = True     # toggling the option for ignoring stopwords
IGNORE_NUMBERS = True       # toggling the option for ignoring numbers
IGNORE_SINGLES = True       # toggling the option for ignoring single character tokens
RECORD_TIME = True         # toggling for recording the time taken for indexer
BYTE_SIZE = 4               # docID is in int

"""
indexer which produces dictionary and postings file
params:
    document_directory: corpus directory for indexing
    dictionary_file:    dictionary of terms
    postings_file:      postings file for all terms in dictionary
    metadata_file:      file to store some metadata used by search.py
"""
def index(document_directory, dictionary_file, postings_file, metadata_file):
    #print('document_directory:' + str(document_directory)); # DEBUG
    # preprocess docID list
    #docID_list = [int(docID_string) for docID_string in os.listdir(document_directory)]
    docID_list = [str(docID_string) for docID_string in os.listdir(document_directory)] # use filename string as docID
    docID_list.sort()
    #print('DEBUG:' + str(docID_list)); # DEBUG

    stemmer = nltk.stem.porter.PorterStemmer()
    stopwords = nltk.corpus.stopwords.words('english')
    #print('DEBUG:' + str(stopwords)); # DEBUG
    docs_indexed = 0    # counter for the number of docs indexed
    dictionary = {}     # key: term, value: docIDs containing term (incudes repeats)
    postingDict = {}    # key: term, value: docID and term frequency pairs; i.e. {str(docID)],int(tf)}; i.e. [{'docID':'doc5.txt','tf':'3'},{'docID':'doc17.txt','tf':'1'}]

    # for each document in corpus
    for docID in docID_list:
        if (LIMIT and docs_indexed == LIMIT): break
        file_path = os.path.join(document_directory, str(docID))
        
        # if valid document
        if (os.path.isfile(file_path)):
            file = codecs.open(file_path, encoding='utf-8', errors='replace')
            document = file.read()                  # read entire document
#            print('DEBUG:document:' + str(document)); # DEBUG
            tokens = nltk.word_tokenize(document)   # list of word tokens from document
#            print('DEBUG:tokens:' + str(list(tokens))); # DEBUG
            bigrm = nltk.bigrams(tokens)            # generate bigrams from single-word tokens (see https://stackoverflow.com/a/47154217 , https://www.tutorialspoint.com/python_text_processing/python_bigrams.htm)
            bigrm_list = list(bigrm);               # must convert bigrm to a list (i.e. can't use 'list(bigrm)' repeatedly)
#            print('DEBUG:bigrm:' + str(list(bigrm_list))); # DEBUG
            bigrm2_list = list(map(' '.join, bigrm_list));
#            print('DEBUG:bigrm2:' + str(list(bigrm2_list))); # DEBUG
            tokens = tokens + bigrm2_list;          # join lists https://appdividend.com/2020/12/02/python-join-list/
#            print('DEBUG:tokens:' + str(tokens)); # DEBUG
#            time.sleep(10); # DEBUG
            
            
            
            # for each term in document
            for word in tokens:
                term = word.lower()         # casefolding
                if (IGNORE_STOPWORDS and term in stopwords):    continue    # if ignoring stopwords
                if (IGNORE_NUMBERS and is_number(term)):        continue    # if ignoring numbers
                term = stemmer.stem(term)   # stemming
                if (term[-1] == "'"):
                    term = term[:-1]        # remove apostrophe
                if (IGNORE_SINGLES and len(term) == 1):         continue    # if ignoring single terms
                if (len(term) == 0):                            continue    # ignore empty terms
                
                # if term not already in dictionary
                if (term not in dictionary):
                    dictionary[term] = [docID]   # define new term in in dictionary (dict dictionary value is list; key is term)
                # else if term is already in dictionary, append docID
                else:
                    dictionary[term].append(docID) # append repeat docID to list (list 'dictionary[term]')
                    
            docs_indexed += 1
            file.close()

    #pprint.pprint(dictionary) # DEBUG
#    print('==END_TOKENIZE==========') # DEBUG


    # open files for writing   
    dict_file = codecs.open(dictionary_file, 'w', encoding='utf-8')
    ####post_file = open(postings_file, 'wb')
    post_file = open(postings_file, 'w')
    meta_file = open(metadata_file, 'w')
  
    # write dictionary file and postings file
    vector_squares_sum = collections.defaultdict(lambda: 0)     # (accumulator) Key: docID, Value: sum of squared weighted components
    ####byte_offset = 0         # byte offset for pointers to postings file
    for term, docs in dictionary.items():  # for <key>, <value> in dictionary.items():
        # term: key of dict dictionary
        # docs: value of dict dictionary (Is list whose elements are names of files containing term; elements may be repeated)
#        print("" + "term:" + str(term)); # DEBUG
#        print("" + "docs:" + str(docs)); # DEBUG
#        print(term); # DEBUG: i.e. 'spider'
#        time.sleep(0.2); # DEBUG
        #print(docs); # DEBUG: i.e. ['doc5.txt', 'doc5.txt', 'doc5.txt', 'doc17.txt', 'doc19.txt', 'doc19.txt', 'doc19.txt', 'doc19.txt', ]
        #time.sleep(1);
        doc_tf = collections.Counter(docs)  # key: docID, Value: occurences of current term in docID list (e.g. "term frequency"; i.e. Counter {'doc5.txt':3, 'doc17.txt':1, 'doc19.txt':4} )
        df = len(doc_tf)                    # document frequency (number of unique docIDs) (e.g. how many unique documents term appears in) (i.e. '3')
        
        # for each (docID, tf) pair indexed
        for docID, tf in sorted(doc_tf.items()):  # for <key>, <value> in dictionary sorted(doc_tf.items())
#            print("  " + "docID:" + str(docID)); # DEBUG
#            print("  " + "tf   :" + str(tf)); # DEBUG
            # docID: key of dict sorted(doc_tf.items()); i.e. 'doc5.txt'; note: this for loop runs for as many docIDs that are in doc_tf
            # tf: key of dict sorted(doc_tf.items()); i.e. '3'
            w_td = 1 + math.log(tf, 10) # tf as weighted vector component (i.e. log-frequency weight (w) of term t in document d)
            # accumulate squared weighted components for docID (grows as each term is considered)
            vector_squares_sum[docID] += w_td ** 2 # for each docID, calculate square of log-frequency weight (for later summing in search.py)
#            print('vector_squares_sum[' + str(docID) + ']:' + str(vector_squares_sum[docID]))
            # write posting, tf pair into postings file
            ####posting = struct.pack('II', docID, tf) # Pack int(docID), int(tf) as unsigned integer 
            ####post_file.write(posting)

            # Construct postingDict as dict of list of tuples
            #  dict key:str(term), value:list(dict())
            #  tuple: (docID, tf)
            postingEntry = (str(docID),int(tf))
#            print("term:" + term); # DEBUG
            if (term not in postingDict):
                postingDict[term] = [(str(docID),int(tf))] # create list postingDict[term] and save tuple
            else:
                postingDict[term].append(postingEntry) # append tuple to list postingDict[term]
            
#            pprint.pprint(postingDict, width=200); # DEBUG
#            print("====END_postingDict============"); # DEBUG
            #time.sleep(0.2); # DEBUG
        
        # write to dictionary file and update byte offset; NOTE: byte_offset correlated with term and tf
        ####dict_file.write(term + " " + str(df) + " " + str(byte_offset) + "\n")
        dict_file.write(term + ";" + str(df) + "\n") # i.e. 'str(term) appears in int(df) unique documents; str(term)'s <docID,tf> start in post_file at byte offset int(byte_offset)'
        ####byte_offset += 2 * BYTE_SIZE * df   # 2 Integers for each implicit (docID, tf) pair
#        print('===END_TERM=============') # DEBUG

    # convert dict postingDict to JSON and write to post_file
    with post_file as file:
        file.write(json.dumps(postingDict))

    # convert dict vector_squares_sum to JSON and write to meta_file
    with meta_file as file:
        file.write(json.dumps(vector_squares_sum))
        
    # writes metadata which contains docIDs indexed and also their respective vector lengths before taking sqrt (____REMOVE SINCE replaced by writes to `meta_file`???_______)
    meta_info = "metadata:"
    meta_list = sorted(vector_squares_sum.items())
#    print(meta_list); # DEBUG
#    print('==END_meta_list=========='); # DEBUG
    for i in range(len(meta_list)):
        info = meta_list[i]
        meta_info += str(info[0]) + ":" + str(info[1])    # <docID>:<vector squares sum>
        if (i != len(meta_list) - 1):  # append ',' unless last loop 
            meta_info += ","
#        print(meta_info); # DEBUG
#        print('===END_meta_info_loop'); # DEBUG
#    print('==END_meta_info'); # DEBUG
    dict_file.write(meta_info)

    # close files
    dict_file.close()
    post_file.close()

"""
This function is modified from: http://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-in-python
returns True if the token is a number else false
param:
    token:  token string
"""
def is_number(token):
    token = token.replace(",", "")  # ignore commas in token
    # tries if token can be parsed as float
    try:
        float(token)
        return True
    except ValueError:
        return False

"""
prints the proper command usage
"""
def print_usage():
    print ("usage: " + sys.argv[0] + " -i directory-of-documents -d dictionary-file -p postings-file -m metadata-file")

document_directory = dictionary_file = postings_file = metadata_file = None
try:
    opts, args = getopt.getopt(sys.argv[1:], 'i:d:p:m:')

# if illegal arguments, print usage to user and exit
except (getopt.GetoptError, err):
    print_usage()
    sys.exit(2)

# for each option parsed
for o, a in opts:
    if o == '-i':
        document_directory = a
    elif o == '-d':
        dictionary_file = a
    elif o == '-p':
        postings_file = a
    elif o == '-m':
        metadata_file = a
    else:
        assert False, "unhandled option"

# if missing out on an argument, print usage to user and exit
if document_directory == None or dictionary_file == None or postings_file == None or metadata_file == None:
    print_usage()
    sys.exit(2)

if (RECORD_TIME): start = timeit.default_timer()                              # start time
index(document_directory, dictionary_file, postings_file, metadata_file)   # call the indexer
if (RECORD_TIME): stop = timeit.default_timer()                               # stop time
if (RECORD_TIME): print ('Indexing time:' + str(stop - start))                # print time taken
