#!/usr/bin/env python3
# Desc: Generates search results from index files produced by index.py
# Input: queries, dictionary, metadata, and postings files. Also, paths for all
#   input and output files.
# Output: output file

# Ref/Attrib: "8 7 Calculating TF IDF Cosine Scores 12 47"; Stanford CS 124/LINGUIST 180 - Introduction to Information Retrieval; https://www.youtube.com/watch?v=k1tD7pYKWuM&list=PLaZQkZp6WhWwoDuD6pQCmgVyDbUWl_ZUi&index=13

import collections
import operator
import getopt
import codecs
import struct
import timeit
import math
import nltk
import sys
import io
import os
import re
import json
import time # DEBUG
import pprint # DEBUG

IGNORE_STOPWORDS = True     # toggling the option for ignoring stopwords
IGNORE_SINGLES = True       # toggling the option for ignoring single character tokens
RECORD_TIME = True          # toggling for recording the time taken for indexer
BYTE_SIZE = 4               # docID is in int

# configurable option
top_limit = 10              # number of most relevant docIDs to fetch

"""
conducts boolean queries from queries_file and writes outputs to output_file
params:
    dictionary_file:    dictionary file produced by indexer
    document_directory: corpus directory for retrieving context snippets
    postings_file:      postings file produced by indexer
    queries_file:       file of boolean queries
    output_file:        responses to boolean queries
"""
def search(dictionary_file, document_directory, postings_file, queries_file, metadata_file, output_file):
    # print('dictionary_file:' + str(dictionary_file)); # DEBUG
    # print('postings_file:' + str(postings_file));     # DEBUG
    # print('queries_file:' + str(queries_file));       # DEBUG
    # print('metadata_file:' + str(metadata_file));     # DEBUG
    # print('output_file:' + str(output_file));         # DEBUG
    # open files
    dict_file = codecs.open(dictionary_file, encoding='utf-8', errors='replace')
    ####post_file = io.open(postings_file, 'rb')
    post_file = open(postings_file, 'r'); # NEW
    query_file = codecs.open(queries_file, encoding='utf-8', errors='replace')
    meta_file = io.open(metadata_file, 'r'); # NEW
    out_file = open(output_file, 'w')

    # load dictionary to memory
    loaded_dict = load_dictionary(dict_file)
    dictionary = loaded_dict         # dictionary map
#    pprint.pprint(dictionary, width=200); # DEBUG
#    print('===END_dictionary========='); # DEBUG
    dict_file.close();
    
    # load metadata to memory
    #### vector_squares_sum = loaded_dict[1] # Key: docID, Value: squared log-weighted freq w_td components
    vector_squares_sum = load_metadata(meta_file);
#    print('===END_vector_squares_sum============'); # DEBUG
    meta_file.close();

    # load postings to memory
    postingDict = load_posting_list2(post_file);
    
    # process each query
    queries_list = query_file.read().splitlines()
#    print(queries_list); # DEBUG
#    print('===END_queries_list=========='); # DEBUG
    outputList = [];
    queryResultsList = [];
    termsMatched = [];
    for i in range(len(queries_list)):
#        print('i:' + str(i))
        queryResultsDict = {};
        queryResultsList = [];
        query = queries_list[i]
        query_csv1 = query.split(",")[0] # first CSV field (user-specified query id)
        query_csv2plus = query.split(",")[1:] # fields after first CSV field
        query = query.split(",")[1:] # split query into list using comma delimiters and remove first element (query id)
        query = ",".join(query) # convert trimmed query list back into query string (sans query id)
#        print('query_csv2plus:' + str(query_csv2plus)); # DEBUG
#        print('query:' + str(query)); # DEBUG
#        time.sleep(1); # DEBUG
        result, matchedTerms = get_top_cosine_scores(query, dictionary, postingDict, vector_squares_sum)
#        print('matchedTerms:' + str(matchedTerms))
#        print('result:' + str(result)); # DEBUG
#        print('====END_result======='); # DEBUG
       
        # write each result to output
        for j in range(len(result)):
            docID = ''
            score = ''
            document = ''
            snippet = ''
            file_path = ''
#            print('j:' + str(j))
            resultDict = {}; # reset resultDict
            docID = str(result[j][0])
            score = str(result[j][1])
            # get document snippet
            file_path = os.path.join(document_directory, str(docID))
            ## if valid document
            if (os.path.isfile(file_path)):
                file = codecs.open(file_path, encoding='utf-8', errors='replace')
                document = file.read()   # read entire document
                snippet = document[:60] # trim to first 60 chars
            resultDict["resultRank"] = j
            resultDict["docID"] = docID
            resultDict["docSnippet"] = snippet
            resultDict["score"] = score
            queryResultsList += [resultDict]
            #print(queryResultsList)
        queryResultsDict["query"] = query  # comment out if queries are long
        queryResultsDict["query_csv1"] = query_csv1
        #queryResultsDict["query_csv2-"] = query_csv2plus[0][:60]
        queryResultsDict["matchedTerms"] = matchedTerms
        queryResultsDict["resultList"] = queryResultsList
        outputList += [queryResultsDict]
#    pprint.pprint(outputList) # DEBUG
    # convert list outputList to JSON and write to out_file
    with out_file as file:
        file.write(json.dumps(outputList, indent=4)) # for output list of dicts to JSON, see https://stackoverflow.com/a/21525380
        print('STATUS:Results written to:' + str(output_file))
    
#        print('====END_for_loop_iter_len(queries_list)========'); # DEBUG

    # close files
    post_file.close()
    query_file.close()
    out_file.close()

"""
returns 1-tuple of dictionary loaded with term:df key:value pairs
params:
    dict_file: opened dictionary file
"""
def load_dictionary(dict_file):
    dictionary = {}             # dictionary map loaded
    ####vector_squares_sum = {}     # Key: docID, Value: square of weighted component

    # load each term along with its df and postings file pointer to dictionary
    for entry in dict_file.read().split('\n'):
        tokens = entry.split(";") # dictionary line elements are delimited by semicolon (';')
        # process dictionary entries
       #### if (len(tokens) == 3):
        if (len(tokens) == 2):
            term = tokens[0]
            df = int(tokens[1])
            #### offset = int(tokens[2]) # byte offset in post_file where term <int(docID),int(tf)> data begins
            #### dictionary[term] = (df, offset) # save retrieved int(df) and int(offset) as tuple in dict dictionary against key str(term)
            dictionary[term] = df # save retrieved int(df) in dict dictionary against key str(term)
        # # process metadata #______NEEDS_ATTENTION
        # else:
        #     line = tokens[0]
        #     for pair in line[9:-1].split(','):
        #         pair = pair.split(":")
        #         docID = int(pair[0])
        #         sum_squares = float(pair[1])
        #         vector_squares_sum[docID] = sum_squares
#    pprint.pprint(dictionary,width=200); # DEBUG
#    print('====END_load_dictionary()=================='); # DEBUG
    return dictionary

"""
returns 1-tuple of metadata
params:
    meta_file: opened metadata file
"""
def load_metadata(meta_file):
    vector_squares_sum = {};
    vector_squares_sum = json.load(meta_file);
#    pprint.pprint(vector_squares_sum, width=200); # DEBUG
#    print('====END_vector_squares_sum'); # DEBUG
#    print('===END_load_metadata()============================'); # DEBUG
    return vector_squares_sum

"""
returns the top_limit most relevant docIDs in the result for the given query
params:
    query:          the query string e.g. 'China's economic progress'
    dictionary:     the dictionary in memory
    indexed_docIDs: the list of all docIDs indexed
"""
def get_top_cosine_scores(query, dictionary, postingDict, vector_squares_sum):
    # desc: Generate a search result from a given query
    # output: most_relevant
    #         matchedTerms
    matchedTerms = [];
    scores = collections.defaultdict(lambda: 0) # (accumulator) Key: docID, Value: raw tabulated score (before cosine normalization)
    N = len(vector_squares_sum) # number of documents in collection
#    print('len(vector_squares_sum):' + str(N)); # DEBUG
#    print('vector_squares_sum:' + str(vector_squares_sum)); # DEBUG

    # for each query term
    for term, tf_raw_query in get_query_terms(query):
        if (term not in dictionary):
            continue    # skip terms not in dictionary
        else:
            matchedTerms.append(term);
        tf_wt_query = 1 + math.log(tf_raw_query, 10) # log-frequency weight factor; see 'l' in 'qqq' in 'lnc-ltc' in 'ddd.qqq' notation
        df = dictionary[term] # get dictionary frequency of term (of N documents, term present in df of them)
#        print('term:' + str(term)); # DEBUG
#        print('dictionary[term]=df=' + str(df)); # DEBUG
        idf = math.log(N/float(df), 10) # inverse document frequency factor
        w_tq = tf_wt_query * idf    # weight for term in query (see 't' in 'qqq' in 'lnc-ltc' in 'ddd-qqq' notation) #fix????______

        # for each document in postings list
        ####for docID, tf_raw_document in load_posting_list(post_file, df, dictionary[term][1]):
        ##### desc: get <docID,tf> from post_file using file path (post_file), offset (dictionary[term][1]), and how many iterations to read set number of bytes from file (df=dictionary[term][0])
        # desc: get <docID,tf> from post_file using post_file
        for docID, tf_raw_document in postingDict[term]:  # Simply get <docID,tf> from postingsDict using str(term)
            tf_wt_doc = 1 + math.log(tf_raw_document, 10) # see 'l' in 'ddd' in 'lnc-ltc' in 'ddd.qqq' notation
            w_td = tf_wt_doc        # weight for term in current document (see 'n' in 'ddd' in 'lnc-ltc' in 'ddd.qqq' notation) #fix????_______
            scores[docID] += (w_tq * w_td) # accumulate scores for each doc # (note: part of dot product e.g. cosine)

#    print('scores.items()')
#    print(scores.items())
    for docID, score in scores.items():
        length = math.sqrt(vector_squares_sum[docID]) # Calculate normalization length for each docID
        scores[docID] /= length # Normalize term scores for each docID; (note: part of dot product; e.g. cosine) #___fix???_Normalize_w_tq__w_td_separately???

#    print(scores)
    # sorts list and return most relevant documents
    most_relevant = sorted(scores.items(), key=operator.itemgetter(1), reverse=True)
#    print(most_relevant); # DEBUG
#    time.sleep(2); # DEBUG
#    print('====END_get_top_cosine_scores()==========================='); # DEBUG
    if (len(most_relevant) <= top_limit):
        return most_relevant, matchedTerms
    else:
        return most_relevant[:int(top_limit)], matchedTerms

    
"""
returns a list of (term, tf)
params:
    query:  the query string e.g. 'China's economic progress'
"""
def get_query_terms(query):
    stemmer = nltk.stem.porter.PorterStemmer() # instantiate stemmer; should be same as in index.py
#    print('DEBUG:query:' + str(query)); # DEBUG
    tokens = nltk.word_tokenize(query) # should be same as in index.py
#    print('DEBUG:tokens:' + str(list(tokens))); # DEBUG
    bigrm = nltk.bigrams(tokens)            # generate bigrams from single-word tokens (see https://stackoverflow.com/a/47154217 , https://www.tutorialspoint.com/python_text_processing/python_bigrams.htm)
    bigrm_list = list(bigrm);               # must convert bigrm to a list (i.e. can't use 'list(bigrm)' repeatedly)
#    print('DEBUG:bigrm:' + str(list(bigrm_list))); # DEBUG
    bigrm2_list = list(map(' '.join, bigrm_list));
 #   print('DEBUG:bigrm2:' + str(list(bigrm2_list))); # DEBUG
    tokens = tokens + bigrm2_list;          # join lists https://appdividend.com/2020/12/02/python-join-list/
#    print('DEBUG:tokens:' + str(tokens)); # DEBUG
#    time.sleep(10); # DEBUG


    stopwords = nltk.corpus.stopwords.words('english') # should be same as in index.py
    # populate query_tf
    query_tf = {} # Key: term, Value: tf
    for word in tokens:
        term = word.lower()
        if (IGNORE_STOPWORDS and term in stopwords):    continue    # if ignoring stopwords
        term = stemmer.stem(term)   # stemming
        if (term[-1] == "'"):
            term = term[:-1]        # remove apostrophe
        if (IGNORE_SINGLES and len(term) == 1):         continue    # if ignoring single terms
        if (len(term) == 0):                            continue    # ignore empty terms

        if (term not in query_tf):
            query_tf[term] = 1 # intitilize key:value since term not yet key in dict query_tf
        else:
            query_tf[term] += 1 # increment term's counter (query term frequency) in dict query_tf

#    pprint.pprint(query_tf.items()); # DEBUG
#    print('====END_query_tf.items============='); # DEBUG
#    print('====END_get_query_terms()============================'); # DEBUG
    return query_tf.items()
"""
returns posting list for term corresponding to the given offset
params:
    post_file:  opened postings file
    length:     length of posting list (same as df for the term)
    offset:     byte offset which acts as pointer to start of posting list in postings file
"""
# def load_posting_list(post_file, length, offset):
#     # BK_NOTE: Rewrite to use JSON instead of struct.unpack()
#     # note: length is df (doc frequency), and is reused here to control how many many increments
#     #   of post_file is read.
#     post_file.seek(offset)
#     posting_list = []
#     for i in range(length):
#         posting = post_file.read(2 * BYTE_SIZE)
#         pair = struct.unpack('II', posting) # docID, tf pair
#         posting_list.append(pair)
#     return posting_list

"""
returns posting list for term
params:
    post_file: opened postings file
    term:      term
"""
def load_posting_list2(post_file):
    # input: file(post_file)
    # output: postingDict
#    print('====BEGIN_load_posting_list2()======================='); # DEBUG

    # Regenerate postingDict from JSON-serialized post_file
    postingDict = {};
    postingDict = json.load(post_file);
    #pprint.pprint(postingDict,width=200)
    #print('====END_load_posting_list2()======================='); # DEBUG
    return postingDict

"""
prints the proper command usage
"""
def print_usage():
    print ("usage: " + sys.argv[0] + " -d dictionary-file -i directory-of-documents -p postings-file -q file-of-queries -m metadata-file -o output-file-of-results [-n number-of-results (default 10)]")
    print('''

The <file-of-queries> should have one query-per-line, with a unique
user-specified query ID occupying the first CSV field and the query
text occupying everything after. The query ID (text before the first
comma) is ignored but is included in <output-file-of-results>.

Example queries file:

my_query_1, Dr Doom favorite foods
my_query_2, bake better pizza
my_query_3, world conquer plan weakness
my_query_4, escape plan
moms_query_1, haircut place mom wants for me
moms_query_2, how to fix mom's printer
    ''')

dictionary_file = document_directory = postings_file = queries_file = metadata_file = output_file = None
try:
    opts, args = getopt.getopt(sys.argv[1:], 'd:i:p:q:m:o:n:')
except (getopt.GetoptError, err):
    usage()
    sys.exit(2)
for o, a in opts:
    if o == '-d':
        dictionary_file = a
    elif o == '-i':
        document_directory = a
    elif o == '-p':
        postings_file = a
    elif o == '-q':
        queries_file = a
    elif o == '-m':
        metadata_file = a
    elif o == '-o':
        output_file = a
    elif o == '-n':
        top_limit = int(a)
    else:
        assert False, "unhandled option"
if (dictionary_file == None or document_directory == None or postings_file == None or queries_file == None or metadata_file == None or output_file == None):
    print_usage()
    sys.exit(2)

if (RECORD_TIME): start = timeit.default_timer()                    # start time
search(dictionary_file, document_directory, postings_file, queries_file, metadata_file, output_file)   # call the search engine on queries
if (RECORD_TIME): stop = timeit.default_timer()                     # stop time
if (RECORD_TIME): print ('Querying time:' + str(stop - start))      # print time taken
